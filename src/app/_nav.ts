import { INavData } from '../../node_modules/@coreui/angular/lib/sidebar/public_api';

export const navItems: INavData[] = [
  {
    name: 'Dashboard',
    url: '/dashboard',
    icon: 'fa fa-home'
    // badge: {
    //   variant: 'info',
    //   text: 'NEW'
    // }
  },
  {
    name: 'Tenant',
    url: '/tenant',
    icon: 'fa fa-user'
  }
  // {
  //   title: true,
  //   name: 'Title'
  // },
  // {
  //   name: 'Disabled',
  //   url: '/dashboard',
  //   icon: 'icon-ban',
  //   attributes: { disabled: true },
  // }
];
