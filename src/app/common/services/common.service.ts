import { Injectable, ViewChild } from '@angular/core';
import { ToasterService } from 'angular2-toaster';
import { AppComponent } from '../../app.component';

@Injectable({
  providedIn: 'root'
})
export class CommonService {

  constructor(private toasterService: ToasterService) { }
    // #region Toaster Message

    popToast(Type?, Title?, Message?) {
      this.toasterService.pop(Type, Title, Message);
    }

    displayToast(type: number, message?: any, title?: any) {
      let typeString;
      switch (type) {
        case 1: {
          typeString = "success";
          break;
        }
        case 2: {
          typeString = "error";
          break;
        }
        case 3: {
          typeString = "warning";
          break;
        }
        case 4: {
          typeString = "info";
          break;
        }
        default: {
          typeString = "error";
          break;
        }
      }
      title = title ? title : null;
      this.popToast(typeString, title, message);
    }
    // #endregion

    apiError(err){
      this.displayToast(2, err.message);
    }
}


