import { Component, OnInit } from "@angular/core";
import { Router, NavigationEnd } from "@angular/router";
import { ToasterConfig, ToasterService } from 'angular2-toaster';

@Component({
  // tslint:disable-next-line
  selector: "body",
  template: `<router-outlet></router-outlet>
    <toaster-container [toasterconfig]="config"></toaster-container>`,
})
export class AppComponent implements OnInit {
  title = "CoreUI 2 for Angular 8";
  constructor(private router: Router) {}

  ngOnInit() {
    this.router.events.subscribe((evt) => {
      if (!(evt instanceof NavigationEnd)) {
        return;
      }
      window.scrollTo(0, 0);
    });
  }

  // #region Toaster
  config: ToasterConfig = new ToasterConfig({
    showCloseButton: true,
    tapToDismiss: false,
    timeout: 3000,
    animation: "flyRight",
    limit: 5,
    preventDuplicates: false,
    newestOnTop: true,
    positionClass: "toast-top-right",
  });
  // #endregion
}
