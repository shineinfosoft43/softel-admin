import { ManageQuestionsComponent } from './manage-questions/manage-questions.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddEditTenantComponent } from './add-edit-tenant/add-edit-tenant.component';
import { TenantDetailsComponent } from './tenant-details/tenant-details.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'tenant-details',
    data: {
      title: 'Tenant'
    }
  },
  {
    path: 'tenant-details',
    component: TenantDetailsComponent,
    data: {
      title: 'Tenant-Details'
    }
  },
  {
    path: 'add-tenant',
    component: AddEditTenantComponent,
    data: {
      title: 'Tenant'
    }
  },
  {
    path: 'manage-questions',
    component: ManageQuestionsComponent,
    data: {
      title: 'Manage Questions'
    }
  },
  {
    path: 'manage-batches/:ids',
    loadChildren: () => import('./manage-batches/manage-batches.module').then(batche => batche.ManageBatchesModule),
    data: {
      title: 'Manage Batches'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TenantRoutingModule { }
