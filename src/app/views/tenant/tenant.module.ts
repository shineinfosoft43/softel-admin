import { CommonModule } from '@angular/common';
import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { TenantComponent } from './tenant.component';
import { TenantRoutingModule } from './tenant-routing.module';
import { TenantDetailsComponent } from './tenant-details/tenant-details.component';
import { PaginationModule } from 'ngx-bootstrap/pagination';
import { AddEditTenantComponent } from './add-edit-tenant/add-edit-tenant.component';
import { ManageQuestionsComponent } from './manage-questions/manage-questions.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxSpinnerModule } from 'ngx-spinner';
import { SearchPipe } from '../../common/pipe/search.pipe';
import { NgxPaginationModule } from 'ngx-pagination';
import { DndModule } from 'ngx-drag-drop';

@NgModule({
    imports: [
        NgxPaginationModule,
        CommonModule,
        TenantRoutingModule,
        PaginationModule,
        FormsModule,
        ReactiveFormsModule,
        NgxSpinnerModule,
        DndModule
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA],
    declarations: [
        TenantComponent,
        TenantDetailsComponent,
        AddEditTenantComponent,
        ManageQuestionsComponent,
        SearchPipe
      ]
})
export class TenantdModule { }
