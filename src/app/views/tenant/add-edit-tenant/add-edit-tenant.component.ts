import { TenantService } from "./../../../common/services/tenants/tenant.service";
import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Router, ActivatedRoute } from "@angular/router";
import { NgxSpinnerService } from "ngx-spinner";
import { CommonService } from '../../../common/services/common.service';

@Component({
  selector: "app-add-edit-tenant",
  templateUrl: "./add-edit-tenant.component.html",
  styleUrls: ["./add-edit-tenant.component.scss"],
})
export class AddEditTenantComponent implements OnInit {
  event: string = "add";
  eventFlag: boolean = false;
  tenantForm: FormGroup;
  tenantDetails: any;
  client_logo = "";
  customer_logo = "";
  formData = new FormData();
  clientFile = "";
  customerFile = "";
  userId = null;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    private tenantService: TenantService,
    private spinner: NgxSpinnerService,
    private _cS: CommonService
  ) {}

  ngOnInit(): void {
    this.initTenantForm();
    this.event = this.route.snapshot.paramMap.get("event");
    this.formData = new FormData();
    if (this.event == "edit") {
      // console.log("route data--->",this.route.snapshot.paramMap.get('data'))
      this.userId = this.route.snapshot.paramMap.get("data");
      this.getTenant(this.route.snapshot.paramMap.get("data"));
      // this.editTenantForm(this.route.snapshot.paramMap.get('data'));
    }
  }

  // Init Form
  initTenantForm() {
    this.tenantForm = this.formBuilder.group({
      tenantName: ["", Validators.required],
      journeyStartTime: ["", Validators.required],
      phase1Name: ["", Validators.required],
      phase1CompletionMessage: ["", Validators.required],
      phase2Name: ["", Validators.required],
      phase2CompletionMessage: ["", Validators.required],
      phase3Name: ["", Validators.required],
      phase3CompletionMessage: ["", Validators.required],
      clientLogo: [""],
      customerLogo: [""],
      createdby: ["p@p.com"],
      createddatetime: [new Date()],
      theme: ["", Validators.required],
    });
  }

  // edit form init
  editTenantForm(data?) {
    // this.tenantForm.patchValue(data);
    this.tenantForm.controls["tenantName"].patchValue(data.tenantName);
    this.tenantForm.controls["journeyStartTime"].patchValue(
      new Date(data.journeyStartTime)
    );
    this.tenantForm.controls["phase1Name"].patchValue(data.phase1Name);
    this.tenantForm.controls["phase1CompletionMessage"].patchValue(
      data.phase1CompletionMessage
    );
    this.tenantForm.controls["phase2Name"].patchValue(data.phase2Name);
    this.tenantForm.controls["phase2CompletionMessage"].patchValue(
      data.phase2CompletionMessage
    );
    this.tenantForm.controls["phase3Name"].patchValue(data.phase3Name);
    this.tenantForm.controls["phase3CompletionMessage"].patchValue(
      data.phase3CompletionMessage
    );
    this.tenantForm.controls["theme"].patchValue(data.theme);
    if (data.clientLogo && data.clientLogo !== "") {
      this.client_logo = data.clientLogo;
      this.imgSrcClient = data.clientLogo;
    } else {
      this.client_logo = "";
    }
    if (data.customerLogo && data.customerLogo !== "") {
      this.customer_logo = data.customerLogo;
      this.imgSrCustomer = data.customerLogo;
    } else {
      this.customer_logo = "";
    }
    // this.tenantForm.controls['journeyStartTime'].patchValue(data.journeyStartTime);
  }

  // get tenant data for edit
  async getTenant(data?) {
    // console.log("data--->",data)
    this.spinner.show();
    await this.tenantService.getTenantDetail(data).subscribe(
      (res: any) => {
        if (res) {
          // console.log("get tenant--->",res);
          this.tenantDetails = res;
          this.spinner.hide();
          this.editTenantForm(this.tenantDetails);
        }
        // console.log("res------>", this.tenantList);
      },
      (error) => {
        this.spinner.hide();
      }
    );
  }

  // on submit form
  async onSubmit() {
    if (this.event === "add" && this.tenantForm.valid) {
      this.spinner.show();
      const formValue = this.tenantForm.value;
      formValue.clientLogo = this.clientFile;
      formValue.customerLogo = this.customerFile;
      this.formData.append("Data", JSON.stringify(formValue));
      // this.formData.getAll();
      await this.tenantService.addTenant(this.formData).subscribe(
        (res: any) => {
          if (res) {
            console.log(res);
            this.spinner.hide();
            this._cS.displayToast(1, "Add record successfully");
            this.router.navigateByUrl("tenant/tenant-details");
          }
        },
        (error) => {
          this.spinner.hide();
          console.log("error--->", error.message);
        }
      );
    } else if (this.event === "edit" && this.tenantForm.valid) {
      this.spinner.show();
      const formValue = this.tenantForm.value;
      formValue["id"] = this.userId;
      formValue.clientLogo = this.clientFile;
      formValue.customerLogo = this.customerFile;
      this.formData.append("Data", JSON.stringify(formValue));
      this.formData.getAll("");
      // console.log("edit---", this.tenantForm.value);
      await this.tenantService.updateTenant(this.formData).subscribe(
        (res: any) => {
          if (res) {
            console.log(res);
          }
          this.spinner.hide();
          this._cS.displayToast(1, "Update record successfully");
          this.router.navigateByUrl("tenant/tenant-details");
        },
        (error) => {
          this.spinner.hide();
          console.log("error--->", error.message);
        }
      );
    }
  }

  // on image upload to set form data
  imgSrcClient: any;
  imgSrCustomer: any;
  onImageUpload(data, e) {
    let file = e.target.files[0];

    const size = Math.round(e.target.files.size / 1024);
    if (data === "client") {
      if (file.type.match(/image\/*/) != null) {
        if (size > 2000) {
          this._cS.displayToast(2, "Maximum 2MB size");
          this.tenantForm.get("clientLogo").setValue(null);
        } else {
          this.formData.append("clientLogo", file);
          this.clientFile = file.name;
          var reader = new FileReader();
          reader.onload = () => {
            this.imgSrcClient = reader.result;
          };
        }
      } else {
        this._cS.displayToast(2, "Only image File allowed");

        this.tenantForm.get("clientLogo").setValue(null);
      }
    } else if (data === "customer") {
      if (file.type.match(/image\/*/) != null) {
        if (size > 2000) {
          this._cS.displayToast(2, "Maximum 2MB size");
          this.tenantForm.get("customerLogo").setValue(null);
        } else {
          this.formData.append("customerLogo", file);
          this.customerFile = file.name;
          var reader = new FileReader();
          reader.onload = () => {
            this.imgSrCustomer = reader.result;
          };
        }
      } else {
        this._cS.displayToast(2, "Only image File allowed");
        this.tenantForm.get("customerLogo").setValue(null);
      }
    }
  }

  //On Cancel Click Event
  onCancel() {
    this.router.navigateByUrl("tenant/tenant-details");
  }
}
