import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ManageBatchesComponent } from './manage-batches.component';
import { ManageUserComponent } from './manage-user/manage-user.component';

const routes: Routes = [
  {
    path: '',
    component: ManageBatchesComponent
  },
  {
    path: 'manage-user/:userId',
    component: ManageUserComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ManageBatchesRoutingModule { }
