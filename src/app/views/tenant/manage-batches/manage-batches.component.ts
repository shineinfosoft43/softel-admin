import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { CommonService } from '../../../common/services/common.service';
import { TenantService } from '../../../common/services/tenants/tenant.service';

@Component({
  selector: 'app-manage-branches',
  templateUrl: './manage-batches.component.html',
  styleUrls: ['./manage-batches.component.scss']
})
export class ManageBatchesComponent implements OnInit {

  public tenantId: any;
  apiPayload = {
    currentPage: 1,
    page: 1,
    pageSize: 10,
    totalRecords: 0,
    search: "",
    firstEntry: 1,
    lastEntry: 10,
  };
  public tenantUserList = [];
  userForm: FormGroup;
  public tenantData;
  public migrationTypeList = [];
  public formData = new FormData();

  constructor(private tenantService: TenantService,
    private activeRoute: ActivatedRoute,
    private spinner: NgxSpinnerService,
    private fb: FormBuilder,
    private commanService: CommonService) {
    this.tenantId = this.activeRoute.snapshot.paramMap.get("ids");
    this.getMigrationTypeList();
    this.getTenantDetails();

    this.userForm = this.fb.group({
      file: ['', Validators.required],
      migrationType: ['', Validators.required]
    });
  }

  ngOnInit(): void {
    this.getUserList();
  }

  pageChanged(event) {
    this.apiPayload.page = event;
    this.getUserList();
  }

  // upload CSV detail
  submitUserDetail() {
    if (this.userForm.valid) {
      this.spinner.show();
      const payload = {
        JourneyPathID : this.userForm.controls.migrationType.value,
        TenantID : this.tenantId,
        BatchName : this.tenantData.tenantName,
        CreatedBy: this.tenantData.tenantName,
        Createddatetime: new Date()
      }
      this.formData.append('Data', JSON.stringify(payload));

      this.tenantService.addTenantUserCSV(this.formData).subscribe(
        (res: any) => {
          if (res) {
            this.spinner.hide();
            this.userForm.reset();
            this.commanService.displayToast(1, "Add record successfully");
            this.getUserList();
          }
        },
        (error) => {
          this.spinner.hide();
          console.log("error--->", error.message);
        }
      );
    }
  }

  // upload image
  imageHandler(e) {
    let file = e.target.files[0];
    this.formData.append("File", file);
  }

  // get migration type list
  getMigrationTypeList() {
    this.tenantService.getMigrationType(this.tenantId).subscribe((res: any) => {
      this.migrationTypeList = res;
    });
  }

  // get tenant details
  async getTenantDetails() {
    this.spinner.show();
    await this.tenantService.getTenantDetail(this.tenantId).subscribe((res: any) => {
      if (res) {
        this.tenantData = res;
        this.spinner.hide();
      }
    });
  }

  // get user list
  async getUserList() {
    this.spinner.show();
    await this.tenantService.fetchTenantUserList(this.tenantId, this.apiPayload.page, this.apiPayload.pageSize, this.apiPayload.search).subscribe((res: any) => {
      if (res) {
        this.tenantUserList = res.results;
        this.apiPayload.totalRecords = res.totalNumberOfRecords;
        this.spinner.hide();
      }
    }, error => {
      console.log("error--->", error);
    });
  }

  onSearchData() {
    this.apiPayload.page = 1;
    this.getUserList();
  }

  // page size change event
  async onPageSize() {
    this.apiPayload.page = 1;
    this.getUserList();
  }
}
