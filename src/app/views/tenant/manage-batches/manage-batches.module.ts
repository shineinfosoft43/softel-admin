import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ManageBatchesRoutingModule } from './manage-batches-routing.module';
import { NgxPaginationModule } from 'ngx-pagination';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxSpinnerModule } from 'ngx-spinner';
import { DndModule } from 'ngx-drag-drop';
import { ManageBatchesComponent } from './manage-batches.component';
import { PaginationModule } from 'ngx-bootstrap/pagination';
import { ManageUserComponent } from './manage-user/manage-user.component';


@NgModule({
  declarations: [
    ManageBatchesComponent,
    ManageUserComponent
  ],
  imports: [
    CommonModule,
    ManageBatchesRoutingModule,
    NgxPaginationModule,
    PaginationModule,
    FormsModule,
    ReactiveFormsModule,
    NgxSpinnerModule,
    DndModule
  ]
})
export class ManageBatchesModule { }
